name := "ParsingTest"

version := "1.0"

scalaVersion := "2.11.0"

libraryDependencies += "org.scala-lang" % "scala-parser-combinators" % "2.11.0-M4"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"