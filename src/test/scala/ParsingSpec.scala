import org.scalatest.{Matchers, FlatSpec}

/**
 * Created by msubotic on 10/16/2015.
 */
class ParsingSpec extends FlatSpec with Matchers {
/*

  "parser" should "parse only valid atoms" in {
      MojParser.parseAll(MojParser.atom,"validAtom") should be (Atom("validAtom"))

  }

  it should "parse quoted and unquoted lists" in {
    MojParser.parseAll(MojParser.atomList, "(1 2 3)") === AList(Seq(Atom("1"),Atom("2"),Atom("3")))
    MojParser.parseAll(MojParser.qAtomList, "'(prvi drugi)") === QAList(AList(Seq(Atom("prvi"),Atom("drugi"))))
  }
*/
}



//println(MojParser.parseAll(MojParser.atom, "prvi"))
//println(MojParser.parseAll(MojParser.atom, "prvi dasdasd trecoi"))

//println(MojParser.parseAll(MojParser.atomList, "(prvi drugi treci)"))

//println(MojParser.parseAll(MojParser.element, "(prvi drugi treci)"))
//println(MojParser.parseAll(MojParser.element, "asdasd"))

//println(MojParser.parseAll(MojParser.element, "(prvi (drugi treci) cetvrti)"))

//println(MojParser.parseAll(MojParser.qAtomList, "'(prvi drugi)"))
//println(MojParser.parseAll(MojParser.element, "(prvi '(drugi treci) '(peti))"))

//println(MojParser.parseAll(MojParser.program, ""))
//println(MojParser.parseAll(MojParser.program, "(ime)"))
//println(MojParser.parseAll(MojParser.program, "'(ime)"))
//println(MojParser.parseAll(MojParser.program, "(ime1) (ime2) (ime3)"))