import scala.util.parsing.combinator.{Parsers, RegexParsers}

/**
 * Created by msubotic on 10/15/2015.
 */


//abstract class Element



trait AbstractElement

trait Atom extends AbstractElement
trait AtomList extends AbstractElement

trait RegularAtomList extends AtomList
trait QuotedAtomList extends AtomList

trait Literal extends Atom
trait Identifier extends Atom

trait NumericLiteral extends Literal
trait StringLiteral extends Literal

trait PrefixedIdentifier extends Identifier
trait RegularIdentifier extends Identifier


trait Program extends AbstractElement

case class CProgram(expressions: Seq[AbstractElement]) extends Program
case class CAtom(value: String) extends Atom
case class CAList(args: Seq[AbstractElement]) extends RegularAtomList
case class CQAList(list: AtomList) extends QuotedAtomList
case class CStringLiteral(value:String) extends StringLiteral
case class CNumericLiteral(value:String) extends NumericLiteral //ovo zamijeniti sa nekoliko razlicith literala, mora biti apstrkatn
case class CPrefixedIdentifier(prefix: String, value: RegularIdentifier) extends PrefixedIdentifier
case class CRegularIdentifier(value:String) extends RegularIdentifier
//case class CommentLine(comment : String) extends

object MojParser extends RegexParsers {
  def elem = Char

  def pElement : Parser[AbstractElement] = pAtom | pAtomList

  def pAtom : Parser[Atom] = pLiteral | pIdentifier

  def pLiteral : Parser[Literal] = pNumericLiteral | pStringLiteral

  def pNumericLiteral : Parser[NumericLiteral] = """\b\d+\.?\d*\b""".r ^^ { num => CNumericLiteral(num)}
  def pStringLiteral : Parser[StringLiteral] = """"""".r ~>  """[a-zA-Z]+\w*""".r  <~ """"""".r ^^ { value => CStringLiteral(value) }

  def pIdentifier : Parser[Identifier] = pPrefixedIdentifier | pRegularIdentifier

  def pPrefixedIdentifier : Parser[PrefixedIdentifier] = """([\:\&]|(\,\@?))?""".r ~ pRegularIdentifier ^^ { case prefix ~ value => CPrefixedIdentifier(prefix,value)  }
  def pRegularIdentifier : Parser[RegularIdentifier] = """[a-zA-Z]+\w*""".r ^^ { x => CRegularIdentifier(x) }  //hendlati i dasheve i underscoreove

  def pAtomList: Parser[AtomList] = pQAtomList | pRegAtomList
  def pRegAtomList : Parser[RegularAtomList] = "(" ~> rep(pElement) <~ ")" ^^ { listOfAtoms => CAList(listOfAtoms) }
  def pQAtomList : Parser[QuotedAtomList] = """'""".r ~> pAtomList ^^  { aList => CQAList(aList)}

  def pProgram : Parser[Program] = rep(pElement)  ^^ { listOfElements => CProgram(listOfElements)}

}
//LITERALS and identifiers
//LITERALS strings, numerics
//: i & ispred atoma, predznaci brojeva, * na kraju identifiera za special purposes (let)
//  &optional je simbol prije opcionalnih parametara, oni idu na kraj
//  &rest
//  &key keyword parameters
//  : za bindanje keyword parametara
//  #'functionName expands to function object
//  backquoted atomi (ili samo liste) signaliziraju expression u kojemu treba(normalni) ili ne treba(sa zarezom) evaluirati, ako je ,@ onda se expandaju u elemente list
//  ,identifier
//  ,@identifier
/*
atom (numeric, string)
prefixed-atom (: &)
numeric
just numeric 2312
signed numeric  -21
number numeric 2/10
scientific numeric -2e10
decimal numeric 123.321
*/